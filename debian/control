Source: mcpp
Section: devel
Priority: optional
Maintainer: Kiyoshi Matsui <kmatsui@t3.rim.or.jp>
Uploaders: NIIBE Yutaka <gniibe@fsij.org>
Build-Depends: debhelper (>= 10)
Standards-Version: 4.4.0
Homepage: http://mcpp.sourceforge.net/
Vcs-Git: https://salsa.debian.org/debian/mcpp.git
Vcs-Browser: https://salsa.debian.org/debian/mcpp

Package: mcpp
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Suggests: mcpp-doc
Description: Alternative C/C++ preprocessor
 C/C++ preprocessor defines and expands macros and processes '#if',
 '#include' and some other directives.
 .
 mcpp is an alternative C/C++ preprocessor with the highest conformance.
 It supports multiple standards: K&R, ISO C90, ISO C99, and ISO C++98.
 mcpp is especially useful for debugging a source program which uses
 complicated macros and also useful for checking portability of a source.
 .
 Though mcpp could be built as a replacement of GCC's resident
 preprocessor or as a stand-alone program without using library build of
 mcpp, this package installs only a program named 'mcpp' which links
 shared library of mcpp and behaves independent from GCC.

Package: libmcpp0
Section: libs
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Description: Alternative C/C++ preprocessor (shared library)
 This package installs the shared library version of mcpp.

Package: libmcpp-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, libmcpp0 (= ${binary:Version})
Description: Alternative C/C++ preprocessor (development files)
 This package installs the development files for the library version
 of mcpp.

Package: mcpp-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Multi-Arch: foreign
Description: Alternative C/C++ preprocessor (manual)
 This package contains the manual.
